#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import math
from PyQt5.QtWidgets import QWidget, QApplication
from PyQt5.QtGui import QPainter


class Example(QWidget):

    def __init__(self):
        super().__init__()
        self.initUI()
        self.frame = 10  # отступ от верхнего и нижнего краёв окна
        self.a = -10   # начало отрезка
        self.b = 10    # конец отрезка

    def initUI(self):
        self.setWindowTitle('Function')
        self.show()

    def paintEvent(self, e):
        qp = QPainter()
        qp.begin(self)
        self.draw_function(qp, self.a, self.b, self.size().width(), self.size().height()-self.frame)
        qp.end()

    # отрисовываемые функции

    @staticmethod
    def func(x):
        return x * x

    @staticmethod
    def func2(x):
        return math.sin(x)

    @staticmethod
    def func3(x):
        return math.pow(math.sin(x), 2)*math.pow(math.cos(x), 2)

    @staticmethod
    def func4(x):
        return math.sin(x) * x

    # алгоритм

    def draw_function(self, qp, a, b, maxx, maxy):
        ymin = ymax = self.func(a)
        for xx in range(maxx):
            x = a + xx * (b - a) / maxx
            y = self.func(x)
            if y < ymin:
                ymin = y
            if y > ymax:
                ymax = y
        yy1 = (self.func(a) - ymax) * maxy / (ymin - ymax)+self.frame/2
        xx1 = 0
        for xx2 in range(1, maxx):
            x = a + xx2 * (b - a) / maxx
            y = self.func(x)
            yy2 = (y - ymax)*maxy/(ymin-ymax)+self.frame/2
            qp.drawLine(xx1, yy1, xx2, yy2)
            yy1 = yy2
            xx1 = xx2

        qp.drawLine(0, (0 - ymax)*maxy/(ymin-ymax)+self.frame/2, maxx, (0 - ymax)*maxy/(ymin-ymax)+self.frame/2)  # оси
        qp.drawLine(-a*maxx/(b-a), 0, -a*maxx/(b-a), maxy+self.frame)

        for i in range(a, b):
            qp.drawLine((i-a)*maxx/(b-a), (0-ymax)*maxy/(ymin-ymax)+self.frame/2-2,
                        (i-a)*maxx/(b-a), (0-ymax)*maxy/(ymin-ymax)+self.frame/2+2)  # единичные отрезки по Х

        for i in range(int(ymin)-1, int(ymax)+2):
            qp.drawLine(-a*maxx/(b-a)-2, (i-ymax)*maxy/(ymin-ymax)+self.frame/2,
                        -a*maxx/(b-a)+2, (i-ymax)*maxy/(ymin-ymax)+self.frame/2)   # единичные отрезки по Y


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = Example()
    sys.exit(app.exec_())
